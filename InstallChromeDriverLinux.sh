#!/bin/sh
####
# Author: ddavison
# Description: Download the Linux chromedriver into the current directory
####

  #latest=`curl http://chromedriver.storage.googleapis.com/LATEST_RELEASE`
version="`wget -qO- http://chromedriver.storage.googleapis.com/LATEST_RELEASE`"
  #version="2.9"
download_location="http://chromedriver.storage.googleapis.com/$version/chromedriver_linux64.zip"
  #rm /tmp/chromedriver_linux64.zip
wget -P /tmp $download_location
unzip /tmp/chromedriver_linux64.zip -d .
mv ./chromedriver ./chromedriver.linux
chmod u+x ./chromedriver.linux
